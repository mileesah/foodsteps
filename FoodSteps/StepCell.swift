//
//  StepCell.swift
//  FoodSteps
//
//  Created by Jennifer Lius on 12/05/2015.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import UIKit

class StepCell: UITableViewCell {
  
  @IBOutlet weak var stepLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
}
