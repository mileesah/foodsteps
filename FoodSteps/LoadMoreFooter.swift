//
//  LoadMoreFooter.swift
//  FoodSteps
//
//  Created by Jennifer Lius on 16/05/2015.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import UIKit

class LoadMoreFooter: UIView {

  @IBOutlet weak var loadMoreSpinner: UIActivityIndicatorView!

}
