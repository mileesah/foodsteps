//
//  Cuisine.swift
//  FoodSteps
//
//  Created by Melisaa Ferenal on 5/7/15.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import Foundation


enum Cuisine:Int{
    
    case American=1, Mexican, Italian, Chinese, Japanese, Indian
    
    init (){
        self = .American
    }
    
    init?(number: Int){
        switch number{
        case 1: self = .American
        case 2: self = .Mexican
        case 3: self = .Italian
        case 4: self = .Chinese
        case 5: self = .Japanese
        case 6: self = .Indian
        default: return nil
        }
        
    }

    var name:String
        {
        get
        {
            switch self
            {
            case .American: return "American"
            case .Mexican: return "Mexican"
            case .Italian: return "Italian"
            case .Chinese: return "Chinese"
            case .Japanese: return "Japanese"
            case .Indian: return "Indian"
            default: return "American"
            }
        }
    }
    
    var imageName:String
        {
        get
        {
            return self.name.lowercaseString+".png"
        }
    }

    
    static func getCuisines()->[Cuisine]
    {
        return [American, Mexican, Italian, Chinese, Japanese, Indian]
    }
    

    
    
    
}