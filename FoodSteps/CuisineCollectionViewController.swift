
//
//  CuisineCollectionViewController.swift
//  FoodSteps
//
//  Created by Jennifer Lius on 7/05/2015.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import UIKit

class CuisineCollectionViewController: UICollectionViewController {
  
  let reuseIdentifier = "CuisineCell"
  let cuisines: [Cuisine] = Cuisine.getCuisines()
  
  @IBAction func searchAction(sender: UIButton) {
    self.performSegueWithIdentifier("search", sender: sender)
  }
  
  @IBAction func groceryListAction(sender: UIButton) {
    self.performSegueWithIdentifier("groceryList", sender: sender)
  }
  
  func populateLeftNavItems() {
    
    // create and setup grocery list button
    let groceryButton: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
    groceryButton.frame = CGRectMake(0, 0, 20, 20)
    groceryButton.setBackgroundImage(UIImage(named: "groc.png"), forState: UIControlState.Normal)
    groceryButton.addTarget(self, action: "groceryListAction:", forControlEvents: UIControlEvents.TouchUpInside)
    var groceryBarItem: UIBarButtonItem = UIBarButtonItem(customView: groceryButton)
    
    groceryBarItem.accessibilityLabel = " grocery Button"
    groceryBarItem.accessibilityHint = "click to switch to view your grocery list"
    // create and setup search button
    let searchButton: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
    searchButton.frame = CGRectMake(0, 0, 20, 20)
    searchButton.setBackgroundImage(UIImage(named: "searchW.png"), forState: UIControlState.Normal)
    searchButton.addTarget(self, action: "searchAction:", forControlEvents: UIControlEvents.TouchUpInside)
    var searchBarItem: UIBarButtonItem = UIBarButtonItem(customView: searchButton)
    
    searchButton.accessibilityLabel = " search button"
    searchButton.accessibilityHint = "click to switch to recipe searching view"

    // set grocery and search button to navbar left items
    self.navigationItem.setLeftBarButtonItems([groceryBarItem, searchBarItem], animated: true)
  }
  
  func populateRightNavItems() {
    // add in logo
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    populateLeftNavItems()
    populateRightNavItems()
    
    /*self.navigationController?.navigationBar.= UIColor(rgba: "#4a5866")

      self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
      self.navigationController?.navigationBar.shadowImage = UIImage()
    self.navigationController?.navigationBar.barTintColor = UIColor(red: 74, green: 88, blue: 102, alpha: 1)
*/
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = false
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "foodList" {
      let foodListVC = segue.destinationViewController as! FoodSearchTableViewController
      let cell = sender as! UICollectionViewCell
      let indexPath = self.collectionView!.indexPathForCell(cell)
      let selectedCuisine = self.cuisines[indexPath!.row] as Cuisine
      
      foodListVC.title = selectedCuisine.name
      foodListVC.cuisine = selectedCuisine
      foodListVC.showSearch = false
    } else if segue.identifier == "search" {
      let foodListVC = segue.destinationViewController as! FoodSearchTableViewController
      
      foodListVC.title = "Search"
      foodListVC.showSearch = true
    } else if segue.identifier == "groceryList" {
      let groceryListVC = segue.destinationViewController as! GroceryListTableViewController
      
      groceryListVC.title = "Grocery List"
    }
  }
  
  // MARK: UICollectionViewDataSource
  
  override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    return 1
  }
  
  override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return cuisines.count
  }
  
  override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! CuisineCell
    
    let cuisine: Cuisine = cuisines[indexPath.row]
    cell.cuisineImage.accessibilityActivate();
    cell.cuisineImage.accessibilityLabel = cuisine.name;
    cell.cuisineImage.accessibilityHint = "click to view "+cuisine.name+"recipe"
    cell.cuisineImage.image = UIImage(named: cuisine.imageName)
    
    return cell
  }
    
    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String,
        atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
            //1
            switch kind {
                //2
            case UICollectionElementKindSectionHeader:
                //3
                let headerView =
                collectionView.dequeueReusableSupplementaryViewOfKind(kind,
                    withReuseIdentifier: "cuisineHeader",
                    forIndexPath: indexPath)
                    as! UICollectionReusableView
                return headerView
            default:
                //4
                assert(false, "Unexpected element kind")
            }
    }
  
  override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    self.performSegueWithIdentifier("foodList", sender: collectionView.cellForItemAtIndexPath(indexPath))
  }
  
}
