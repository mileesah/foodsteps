//
//  FoodCell.swift
//  FoodSteps
//
//  Created by Jennifer Lius on 11/05/2015.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import UIKit

class FoodCell: UITableViewCell {
  
  @IBOutlet weak var foodImage: UIImageView!
  @IBOutlet weak var foodName: UILabel!
  @IBOutlet weak var spinner: UIActivityIndicatorView!
  
  func startSpinner() {
    spinner.startAnimating()
  }
  
  func stopSpinner() {
    spinner.stopAnimating()
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
}
