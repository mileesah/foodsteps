//
//  Recipe.swift
//  FoodSteps
//
//  Created by admin  on 9/05/2015.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import Foundation

class Recipe
{
    
    
    private var id:String
    private var yield:String!
    private var nutritionEstimates:[NutritionEstimate]!
    private var images:[String:String]!
    private var recipeName:String!
    private var ingredientLines:[String]!
    private var totalTime:String!
    private var numberOfServings:Int
    private var source:[String:String]
    private var flavors:[String:Double]!
    private var rating:Int!

    
  init(id:String, yield:String!,nutritionEstimates:[NutritionEstimate]!,images:[String:String]!,recipeName:String!, ingredientLines:[String]!, totalTime:String!, numberOfServings:Int, source:[String:String], flavors:[String:Double]!, rating:Int!)
    {
        self.id = id
        self.yield = yield
        self.nutritionEstimates = nutritionEstimates
        self.images = images
        self.recipeName = recipeName
        self.ingredientLines = ingredientLines
        self.totalTime = totalTime
        self.numberOfServings = numberOfServings
        self.source = source
        self.flavors = flavors
        self.rating = rating
    }

   
    func getId()->String{
        return id;
    }

    func getIngredientLines()->[String]{
        return ingredientLines;
    }
    
    func getYield()->String{
        return yield;
    }

    
  func getImages()->[String:String]{
        return images;
    }
    
    func getRecipeName()->String{
        return self.recipeName;
    }
    
    func getTotalTime()->String{
        return totalTime;
    }
        
    func getFlavors()->[String:Double]{
        return flavors;
    }
    
    func getRating()->Int{
        return rating;
    }
    
    func getNumberOfServings()->Int{
        return numberOfServings;
    }
    
    func getSource()->String{
        return self.source["sourceRecipeUrl"]!;
    }
    
    func getCalories() -> String{
        for nutrition in self.nutritionEstimates{
            if nutrition.getAttribute().uppercaseString == "ENERC_KCAL" {
                return nutrition.getValue()
            }
            
        }
        return " "
    }
    
    func getTotalTimeInMins()->String{
        
        if totalTime == nil || totalTime.isEmpty {
            return "-"
        }
        
        
        if totalTime == "1 hr" {
            return "1 hr 0"
        }
        
        var result = totalTime.lowercaseString.stringByReplacingOccurrencesOfString(" minutes", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)

        result = result.lowercaseString.stringByReplacingOccurrencesOfString(" min", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        return result;
    }
    
    
    
}


class NutritionEstimate{
    
    private var attribute:String
    private var description:String
    private var value:String
    private var unit:[String:String]
    
    init(attribute:String,description:String,value:String!, unitName:String!, unitAbbr:String!, unitPlural:String!, unitPluralAbr:String!){
        self.attribute = attribute
        self.description = description
        self.value = value
        self.unit = [
            "name" : unitName,
            "abbrevation" : unitAbbr,
            "plural" : unitPlural,
            "pluralAbbreviation" : unitPluralAbr
        ]
        
    }
    
    func getAttribute() -> String{
        return self.attribute
    }
    
    func getDescription() -> String{
        return self.description
    }
    
    func getValue() -> String{
        return self.value
    }
    
    func getUnitName() -> String{
        return self.unit["name"]!
    }
    
    func getUnitAbbrevation() -> String{
        return self.unit["abbrevation"]!
    }
    func getUnitPlural() -> String{
        return self.unit["plural"]!
    }
    func getUnitPluralAbbreviation() -> String{
        return self.unit["pluralAbbreviation"]!
    }
    
    
    
    
    
    
}