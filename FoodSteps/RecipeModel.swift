//
//  RecipeModel.swift
//  FoodSteps
//
//  Created by Melisaa Ferenal on 5/7/15.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class RecipeModel{
    
    
    
    let BASE_URL = "http://api.yummly.com/v1/api/"
    let SEARCH_URL = "recipes"
    let RECIPE_URL = "recipe"
    let REQUIRE_PICTURES:String = "requirePictures"
    let ALLOWED_CUISINE:String = "allowedCuisine[]"
    let APP_ID :String = "6e44c104"
    let APP_KEY :String = "be821ed249ca24c5e483c49f5eee2856"
    let SEARCH_KEY:String =	"q"
//    let maxResult =	 "10"
  
    
    private struct Static
    {
        static var instance: RecipeModel?
    }
    
    
    
    class var sharedInstance: RecipeModel
    {
        if !(Static.instance != nil)
        {
            Static.instance = RecipeModel()
        }
        return Static.instance!
    }
    
    
    private init()
    {
    }
    
    
  func getRecipesFromYummly(keyword: String?, cuisine: Cuisine?, start: String, maxResult: String, errorCallback: (String) -> Void, callback: ([RecipeSummary]) -> Void )-> Void{
      var recipeList:[RecipeSummary] = []
        var params:[String:String] = [
                        "_app_id":APP_ID,
                        "_app_key":APP_KEY,
                        REQUIRE_PICTURES:"true",
                        "start" : start,
                        "maxResult" : maxResult
                        ]
        if keyword != nil {
            params[SEARCH_KEY] = keyword
        }
      
        if  cuisine != nil {
            params[ALLOWED_CUISINE]="cuisine^cuisine-"+cuisine!.name.lowercaseString
        }
        
        Alamofire.request(.GET, BASE_URL+SEARCH_URL, parameters: params).responseJSON { (request, response, data, error) in
           
            if let err = error
            {
                // error in getting data
                println("Error accessing yummly")
                println(error)
                errorCallback("error")
            }
            else if let data: AnyObject = data
            {
                // convert to JSON
                let result = JSON(data)
                
                if let resultArray = result["matches"].array {
                    
                    //3
                    for recipeJSON  in resultArray {
                        
                        
                        var imageUrlBySize:[Int:String] = [:]
                        for (key, object) in recipeJSON["imageUrlsBySize"] {
                            imageUrlBySize[key.toInt()!] = object.stringValue
                        }
                        var sourceDisplayName = recipeJSON["sourceDisplayName"].string
                        var ingredient:[String] = []
                        for (key, object) in recipeJSON["ingredients"] {
                            ingredient.append(object.stringValue)
                        }
                        var id = recipeJSON["id"].string
                        var smallImageUrls:[String] = []
                        for (key, object) in recipeJSON["smallImageUrls"] {
                            smallImageUrls.append(object.stringValue)
                        }
                        var recipeName = recipeJSON["recipeName"].string
                        var totalTimeInSeconds:Int = 0
                      if recipeJSON["totalTimeInSeconds"] != nil{
                        totalTimeInSeconds = recipeJSON["totalTimeInSeconds"].int!
                      }
                        var courses:[String] = []
                        for (key, object) in recipeJSON["attributes"]["course"] {
                            courses.append(object.stringValue)
                        }
                        var cusineStr:[String] = []
                        for (key, object) in recipeJSON["attributes"]["cuisine"] {
                            cusineStr.append(object.stringValue)
                        }
                        var flavors:[String:Double] = [:]
                        for (key, object) in recipeJSON["flavors"] {
                            flavors[key] = object.doubleValue
                        }
                        var rating:Int = recipeJSON["rating"].int!

                        var recipe = RecipeSummary( imageUrlBySize: imageUrlBySize, sourceDisplayName: sourceDisplayName!, ingredient: ingredient, id: id!, smallImageUrls: smallImageUrls, recipeName: recipeName!, totalTimeInSeconds: totalTimeInSeconds, flavors: flavors, rating: rating, course: courses, cuisine: cusineStr)
                      
                      
                        recipeList.append(recipe)
                    }
            
                }
                
                
                callback(recipeList)

            }
        }

    
    }
    
    
    func getRecipe(id:String, errorCallback:(String) -> Void, callback:(Recipe!) -> Void )-> Void{
      var recipe:Recipe! = nil

        var params:[String:String] = [
            "_app_id":APP_ID,
            "_app_key":APP_KEY,
        ]
        
        var url  =  BASE_URL + RECIPE_URL + "/" + id
        
        Alamofire.request(.GET, url, parameters: params).responseJSON { (request, response, data, error) in
            
            if let err = error
            {
                // error in getting data
                println("Error accessing yummly")
                println(error)
                errorCallback("Error accessing yummly")
            }
            else if let data: AnyObject = data
            {
                // convert to JSON
                let recipeJSON = JSON(data)
                
                
                    //3
                    if recipeJSON  != nil {
                        

                        
                        
                        var id:String = recipeJSON["id"].string!
                        var yield:String = ""
                      if recipeJSON["yield"] != nil{
                        yield = recipeJSON["yield"].string!
                        }
                        var nutritionEstimates:[NutritionEstimate]! = []
                        var nArray:[JSON] = recipeJSON["nutritionEstimates"].array!
                        for nEstimate in nArray {
                            var attribute = ""
                            if nEstimate["attribute"] != nil {
                                attribute = nEstimate["attribute"].string!
                            }
                            
                            var description = ""
                            if nEstimate["description"] != nil {
                                description = nEstimate["description"].string!
                            }
                            
                            var value = ""
                            if nEstimate["value"] != nil {
                                
                                value =  String(stringInterpolationSegment: nEstimate["value"])
                                var d = nEstimate["value"].double!
                                value =   NSString(format: "%.0f", d) as String
                            }
                            
                            var unitName = ""
                            if nEstimate["unit"] != nil || nEstimate["unit"]["name"] != nil  {
                                unitName = nEstimate["unit"]["name"].string!
                            }
                            var unitAbbr = ""
                            if nEstimate["unit"] != nil || nEstimate["unit"]["abbreviation"] != nil  {
                                unitName = nEstimate["unit"]["abbreviation"].string!
                            }
                            
                            var plural = ""
                            if nEstimate["unit"] != nil || nEstimate["unit"]["plural"] != nil  {
                                plural = nEstimate["unit"]["plural"].string!
                            }
                            
                            var pluralAbbreviation = ""
                            if nEstimate["unit"] != nil || nEstimate["unit"]["pluralAbbreviation"] != nil  {
                                pluralAbbreviation = nEstimate["unit"]["pluralAbbreviation"].string!
                            }
                            
                            var n:NutritionEstimate = NutritionEstimate(attribute: attribute, description:  description, value:  value, unitName:  unitName, unitAbbr: unitAbbr, unitPlural:  plural, unitPluralAbr: pluralAbbreviation)
                            nutritionEstimates.append(n)
                        }
                        var images:[String:String]! = [:]
                        var imgArray:[JSON] = recipeJSON["images"].array!
                        for img in imgArray {
                          images["hostedLargeUrl"] = img["hostedLargeUrl"].string
                          images["hostedMediumUrl"] = img["hostedMediumUrl"].string
                          images["hostedSmallUrl"] = img["hostedSmallUrl"].string
                        }
                        var recipeName = recipeJSON["name"].string
                        var ingredientLines:[String]! = []
                        for (key, object) in recipeJSON["ingredientLines"] {
                            ingredientLines.append(object.stringValue)
                        }
                        var totalTime:String = ""
                      if recipeJSON["totalTime"] != nil {
                        totalTime = recipeJSON["totalTime"].string!
                      }
                        var numberOfServings:Int=recipeJSON["numberOfServings"].int!
                        var source:[String:String]=[:]
                        for (key, object) in recipeJSON["source"] {
                            source[key] = object.string
                        }
                        var flavors:[String:Double] = [:]
                        for (key, object) in recipeJSON["flavors"] {
                            flavors[key] = object.doubleValue
                        }
                        var rating:Int = recipeJSON["rating"].int!
                      
                        recipe = Recipe(id: id, yield: yield, nutritionEstimates: nutritionEstimates, images: images, recipeName: recipeName, ingredientLines: ingredientLines, totalTime: totalTime, numberOfServings: numberOfServings, source: source, flavors: flavors, rating: rating)
                    }
                    
                }
          
          if recipe != nil{
            callback(recipe)

          }else{
            errorCallback("Recipe does not exist")
            
          }
          
          
            
        }
        
        
    }
  
  
  
    
    
    
    
}
