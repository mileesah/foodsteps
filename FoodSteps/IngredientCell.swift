//
//  IngredientCell.swift
//  FoodSteps
//
//  Created by Jennifer Lius on 12/05/2015.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import UIKit

class IngredientCell: UITableViewCell {
  
    let groceryModel:GroceryModel = GroceryModel.sharedInstance
    
  @IBOutlet weak var addButton: UIButton!
  @IBOutlet weak var ingredientLabel: UILabel!
  @IBOutlet weak var addHintLbl: UILabel!

  @IBAction func addToGroceryList(sender: UIButton) {
    var str = ingredientLabel.text!
    var regex:NSRegularExpression = NSRegularExpression(pattern: "\\,.*", options: NSRegularExpressionOptions.CaseInsensitive, error: nil)!
    var modString = regex.stringByReplacingMatchesInString(str, options: nil, range: NSMakeRange(0, count(str)), withTemplate: "")
        groceryModel.newGroceryItem(modString, qty: "0")
        println(modString + " added ")
    UIView.animateWithDuration(0.4, animations: { () -> Void in
        self.addButton.alpha = 0
        self.ingredientLabel.alpha = 0
 
    })
    
   
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
        Int64(0.7 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
        UIView.animateWithDuration(1.2, animations: { () -> Void in
            self.addHintLbl.alpha = 1;
            
        })
        UIView.animateWithDuration(1.2, animations: { () -> Void in
            self.addHintLbl.alpha = 0;
            
        })
    }

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
        Int64(2 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                self.addButton.alpha = 1
                self.ingredientLabel.alpha = 1
                
            })
            
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
}
