//
//  Checkbox.swift
//  FoodSteps
//
//  Created by Jennifer Lius on 18/05/2015.
//  Copyright (c) 2015 jeli. All rights reserved.
//
//  Taken from: http://gottliebplanet.com/2014/08/09/create-a-checkbox-control-in-swift/

import UIKit

class Checkbox: UIButton {
  var mDelegate: CheckboxDelegate?

  init(frame: CGRect, selected: Bool) {
    super.init(frame: frame)
    self.selected = selected
    self.adjustEdgeInsets()
    self.applyStyle()
    self.addTarget(self, action: "onTouchUpInside:", forControlEvents: UIControlEvents.TouchUpInside)
  }

  required init(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func adjustEdgeInsets() {
    let lLeftInset: CGFloat = 8.0
    
    self.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
    self.imageEdgeInsets = UIEdgeInsetsMake(0.0 as CGFloat, lLeftInset, 0.0 as CGFloat, 0.0 as CGFloat)
  }
  
  func applyStyle() {
    self.setImage(UIImage(named: "checked_checkbox"), forState: UIControlState.Selected)
    self.setImage(UIImage(named: "unchecked_checkbox"), forState: UIControlState.Normal)
  }
  
  func onTouchUpInside(sender: UIButton) {
    self.selected = !self.selected;
    mDelegate?.didSelectCheckbox(self.selected, identifier: self.tag)
  }
}