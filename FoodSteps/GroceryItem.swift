//
//  GroceryItem.swift
//  FoodSteps
//
//  Created by Melisaa Ferenal on 5/11/15.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import Foundation

import CoreData
@objc(GroceryItem)

class GroceryItem: NSManagedObject {
  @NSManaged var name: String
  @NSManaged var done: Bool
  @NSManaged var qty: String
  
  func getName() -> String {
    return self.name as String
  }
  
  func isDone() -> Bool {
    return self.done
  }
  
  func getQty() -> String {
    return self.qty as String
  }

}