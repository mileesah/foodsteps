//
//  CheckboxDelegate.swift
//  FoodSteps
//
//  Created by Jennifer Lius on 18/05/2015.
//  Copyright (c) 2015 jeli. All rights reserved.
//
//  Taken from: http://gottliebplanet.com/2014/08/09/create-a-checkbox-control-in-swift/

import Foundation

protocol CheckboxDelegate {
  func didSelectCheckbox(state: Bool, identifier: Int)
}