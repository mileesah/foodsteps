//
//  ViewController.swift
//  FoodSteps
//
//  Created by Jennifer Lius on 6/05/2015.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
  var recipeSummaryId: String!
  var recipe: Recipe!
    
    @IBOutlet weak var recipeNameLbl: UILabel!
    @IBOutlet weak var totalTimeLbl: UILabel!
    @IBOutlet weak var nutritionLbl: UILabel!
    @IBOutlet weak var servingsSize: UILabel!
  @IBOutlet weak var foodImageLarge: UIImageView!
  @IBOutlet weak var container: UIView!
  @IBOutlet weak var spinner: UIActivityIndicatorView!
  @IBOutlet weak var containerSpinner: UIActivityIndicatorView!
  
  func loadRecipe() {
    spinner.startAnimating()
    
    let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
    
    dispatch_async(queue) {
      let group = dispatch_group_create()
      
      dispatch_group_async(group, queue) {
        RecipeModel.sharedInstance.getRecipe(self.recipeSummaryId, errorCallback: self.errCallBck, callback: self.callback)
      }
    }
  }
  
  func errCallBck(str: String) -> Void {
    println(str)
  }
  
  func callback(recipe: Recipe!) -> Void {
    self.recipe = recipe
    
    let url = NSURL(string: recipe.getImages()["hostedLargeUrl"]!)
    let data = NSData(contentsOfURL: url!)
    foodImageLarge.image = UIImage(data: data!)
    servingsSize.text = String(self.recipe.getNumberOfServings())
    servingsSize.accessibilityLabel = "Serves "+String(self.recipe.getNumberOfServings())+" people"
    nutritionLbl.text = self.recipe.getCalories()
    nutritionLbl.accessibilityLabel = "Nutrition "+self.recipe.getCalories()+" calories"

    totalTimeLbl.text = self.recipe.getTotalTimeInMins()
    var prepTime =  self.recipe.getTotalTimeInMins().lowercaseString.stringByReplacingOccurrencesOfString(" hr", withString: " hour ", options: NSStringCompareOptions.LiteralSearch, range: nil)
    totalTimeLbl.accessibilityLabel = "Preparation time "+prepTime+" minutes"

    recipeNameLbl.text = self.recipe.getRecipeName().uppercaseString

    spinner.stopAnimating()
    self.view.reloadInputViews()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.title = ""
    self.navigationController!.setNavigationBarHidden(false, animated:true)
    var backBtn:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
    backBtn.addTarget(self, action: "backToRoot:", forControlEvents: UIControlEvents.TouchUpInside)
    backBtn.setImage(UIImage(named: "back.png"), forState: UIControlState.Normal)
    backBtn.frame = CGRectMake(0, 0, 20, 20)
    var myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: backBtn)
    self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
    loadRecipe()
  }
  
    func backToRoot(sender:UIBarButtonItem){
        self.navigationController!.popToRootViewControllerAnimated(true)
    }
    
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
   
}

