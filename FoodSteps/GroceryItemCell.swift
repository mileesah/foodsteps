//
//  GroceryItemCell.swift
//  FoodSteps
//
//  Created by Jennifer Lius on 18/05/2015.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import UIKit

class GroceryItemCell: UITableViewCell {

  @IBOutlet weak var groceryItemLabel: UILabel!
  
}
